Global Payments Webpay (GP) integration.

CONFIGURATION
-------------

After installing module by instructions in INSTALL.txt go to payment
configuration page

  Store Home » Administration » Store » Configuration
  (admin/store/settings/payment/edit/methods)

There is "Global Payments webpay" checkbox which has to be enabled. Next step is
to configure GP credentials.

You will need:

* Gateway URL
* Merchant Number
* Private key (stored on web server)
* Public key (key provided by bank)

You can setup whether is required to complete full payment.

GET INVOLVED
------------

Improvements to this documentation, module or feature requests should be
submitted as issue to project issue queue.

Thanks!
